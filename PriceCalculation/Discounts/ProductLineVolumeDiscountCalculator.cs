﻿using PriceCalculation.Models;
using System;
using System.Collections.Generic;

namespace PriceCalculation.Calculators.Discounts
{
    public class ProductLineVolumeDiscountCalculator
    {
        public IReadOnlyList<Discount> GetProductVolumeDiscounts(IReadOnlyList<ProductLine> productLines)
        {
            var productDiscounts = new List<Discount>();

            foreach (var productLine in productLines)
            {
                var requestProductLine = productLine.RequestLine;

                if (requestProductLine.VolumeDiscount != null)
                {
                    productDiscounts.Add( 
                        new Discount(
                            ownerId: productLine.Id,
                            owner: DiscountOwner.ProductLine,
                            identifier: requestProductLine.VolumeDiscount.Id,
                            name: DiscountName.VolumeDiscount,
                            amount: null,
                            percentage: requestProductLine.VolumeDiscount.Percentage,
                            Array.Empty<DiscountName>()
                        )
                    );
                }
            }

            return productDiscounts;
        }
    }
}
