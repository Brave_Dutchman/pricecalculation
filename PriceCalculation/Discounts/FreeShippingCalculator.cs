﻿using PriceCalculation.Models;
using PriceCalculation.Models.Request;
using System;
using System.Collections.Generic;
using System.Linq;

namespace PriceCalculation.Calculators.Discounts
{
    public class FreeShippingCalculator
    {
        public IReadOnlyList<Discount> GetFreeShippingDiscountIfApplies(
            Shipping shipping,
            IReadOnlyList<ProductLine> productLines, 
            IReadOnlyCollection<DiscountRequest> discounts)
        {
            var results = new List<Discount>();

            if (shipping.MinOrderPriceForFreeShipping != null && shipping.MinOrderPriceForFreeShipping <= productLines.Sum(pl => pl.Price))
            {
                results.Add(_getDiscount(shipping, Guid.NewGuid().ToString(), DiscountName.FreeShippingLinesSum));
            }

            foreach (var discount in discounts)
            {
                if (discount.HasFreeShipping)
                {
                    results.Add(_getDiscount(discount, shipping));
                }

                if (discount.Scope == DiscountScope.Order && discount.ScopeIncludesShipping && discount.IsAmountPercentage && discount.Amount == 100M)
                {
                    results.Add(_getDiscount(discount, shipping));
                }
            }

            return results;
        }

        private Discount _getDiscount(DiscountRequest discount, Shipping shipping)
        {
            return _getDiscount(shipping, discount.Id, DiscountName.FreeShipping, discount.DisablesTypes());
        }

        private Discount _getDiscount(Shipping shipping, string id, DiscountName discountName, IReadOnlyCollection<DiscountName>? disablesTypes = null)
        {
            return new Discount(
                ownerId: shipping.Id,
                owner: DiscountOwner.Shipping,
                identifier: id,
                name: discountName,
                amount: shipping.Price,
                percentage: null,
                disablesTypes: disablesTypes ?? Array.Empty<DiscountName>()
            );
        }
    }
}
