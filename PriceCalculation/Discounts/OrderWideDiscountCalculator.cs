﻿using PriceCalculation.Models;
using PriceCalculation.Models.Request;
using System.Collections.Generic;
using System.Linq;

namespace PriceCalculation.Calculators.Discounts
{
    public class OrderWideDiscountCalculator
    {
        public IReadOnlyList<Discount> GetOrderWideDiscounts(
            IReadOnlyCollection<DiscountRequest> discountRequets,
            IReadOnlyList<ProductLine> productLines,
            Shipping shipping,
            ProductionSpeed productionSpeed,
            PaymentMethod paymentMethod)
        {
            var discounts = new List<Discount>();

            var orderWideDiscounts = discountRequets.Where(d => d.Scope == DiscountScope.Order).ToList();

            foreach (var discount in orderWideDiscounts)
            {
                if (discount.IsAmountPercentage)
                {
                    discounts.AddRange(_applyAmountPercentageDiscount(discount, productLines, shipping, productionSpeed, paymentMethod));
                }
                else
                {
                    discounts.AddRange(_applyFixedAmountDiscount(discount, productLines, shipping, productionSpeed, paymentMethod));
                }
            }

            return discounts;
        }

        private IReadOnlyList<Discount> _applyAmountPercentageDiscount(
            DiscountRequest discount,
            IReadOnlyList<ProductLine> productLines,
            Shipping shipping, 
            ProductionSpeed productionSpeed,
            PaymentMethod paymentMethod)
        {
            var discounts = new List<Discount>();

            foreach (var productLine in productLines)
            {
                discounts.Add(_getDiscount(discount, productLine.Id, DiscountOwner.ProductLine, percentage: discount.Amount));
            }

            discounts.Add(_getDiscount(discount, shipping.Id, DiscountOwner.Shipping, percentage: discount.Amount));
            discounts.Add(_getDiscount(discount, productionSpeed.Id, DiscountOwner.ProductionSpeed, percentage: discount.Amount));
            discounts.Add(_getDiscount(discount, paymentMethod.Id, DiscountOwner.PaymentMethod, percentage: discount.Amount));

            return discounts;
        }

        private IReadOnlyList<Discount> _applyFixedAmountDiscount(DiscountRequest discount,
            IReadOnlyList<ProductLine> productLines,
            Shipping shipping,
            ProductionSpeed productionSpeed,
            PaymentMethod paymentMethod)
        {
            var discounts = new List<Discount>();

            int totalAffecedDiscountHolders = productLines.Count;

            if (shipping.Price > 0)
            {
                totalAffecedDiscountHolders++;
            }

            if (productionSpeed.Price > 0)
            {
                totalAffecedDiscountHolders++;
            }

            if (paymentMethod.Price > 0)
            {
                totalAffecedDiscountHolders++;
            }

            var amountPerItem = discount.Amount / totalAffecedDiscountHolders;

            foreach (var productLine in productLines)
            {
                discounts.Add(_getDiscount(discount, productLine.Id, DiscountOwner.ProductLine, amount: amountPerItem));
            }

            if (shipping.Price > 0)
            {
                discounts.Add(_getDiscount(discount, shipping.Id, DiscountOwner.Shipping, amount: amountPerItem));
            }

            if (productionSpeed.Price > 0)
            {
                discounts.Add(_getDiscount(discount, productionSpeed.Id, DiscountOwner.ProductionSpeed, amount: amountPerItem));
            }

            if (paymentMethod.Price > 0)
            {
                discounts.Add(_getDiscount(discount, paymentMethod.Id, DiscountOwner.PaymentMethod, amount: amountPerItem));
            }

            return discounts;
        }

        private Discount _getDiscount(DiscountRequest discount, string ownerId, DiscountOwner owner, decimal? amount = null, decimal? percentage = null)
        {
            return new Discount(
                ownerId: ownerId,
                owner: owner,
                identifier: discount.Id,
                name: discount.DiscountName,
                amount: amount,
                percentage: percentage,
                disablesTypes: discount.DisablesTypes()
            );
        }
    }
}
