﻿using PriceCalculation.Models;
using PriceCalculation.Models.Request;
using System.Collections.Generic;
using System.Linq;

namespace PriceCalculation.Calculators.Discounts
{
    public class ProductLineDiscountCalculator
    {
        public IReadOnlyList<Discount> GetProductDiscounts(IReadOnlyCollection<DiscountRequest> discounts, IReadOnlyList<ProductLine> productLines)
        {
            var productDiscounts = new List<Discount>();

            foreach (var discount in discounts.Where(t => t.IsAmountPercentage))
            {
                foreach (var line in _productLineDiscounts(discount, productLines))
                {
                    productDiscounts.Add(_getDiscount(discount, line.Id, percentage: discount.Amount));
                }
            }

            foreach (var discount in discounts.Where(t => !t.IsAmountPercentage))
            {
                var productLinesWithDiscount = _productLineDiscounts(discount, productLines).ToList();

                foreach (var line in productLinesWithDiscount)
                {
                    productDiscounts.Add(_getDiscount(discount, line.Id, amount: discount.Amount));
                }
            }

            return productDiscounts;
        }

        private IEnumerable<ProductLine> _productLineDiscounts(DiscountRequest discount, IReadOnlyList<ProductLine> productLines)
        {
            foreach (var productLine in productLines)
            {
                if (productLine.RequestLine.Tags.ContainsAny(discount.Tags))
                {
                    yield return productLine;
                }
            }
        }

        private Discount _getDiscount(DiscountRequest discount, string productId, decimal? amount = null, decimal? percentage = null)
        {
            return new Discount(
                ownerId: productId,
                owner: DiscountOwner.ProductLine,
                identifier: discount.Id,
                name: discount.DiscountName,
                amount: amount,
                percentage: percentage,
                disablesTypes: discount.DisablesTypes()
            );
        }
    }
}
