﻿using PriceCalculation.Models;
using PriceCalculation.Models.Request;
using PriceCalculation.Models.Result;
using PriceCalculation.Services;
using System.Collections.Generic;
using System.Linq;

namespace PriceCalculation
{
    public partial class Calculator
    {
        private readonly TaxRateService _taxRateService;
        private readonly ProductLineService _productLineService;
        private readonly ShippingService _shippingService;
        private readonly PaymentMethodService _paymentMethodService;
        private readonly ProductionSpeedService _productionSpeedService;
        private readonly DiscountService _discountService;
        private readonly CalculationResultService _calculationResultService;

        public Calculator(
            TaxRateService taxRateService,
            ProductLineService productLineService, 
            ShippingService shippingService,
            PaymentMethodService paymentMethodService,
            ProductionSpeedService productionSpeedService,
            DiscountService discountService,
            CalculationResultService calculationResultService)
        {
            _productLineService = productLineService;
            _shippingService = shippingService;
            _paymentMethodService = paymentMethodService;
            _productionSpeedService = productionSpeedService;
            _discountService = discountService;
            _calculationResultService = calculationResultService;
            _taxRateService = taxRateService;
        }

        public CalculationResult Calculate(CalculationRequest request)
        {
            var taxRateMap = new Dictionary<TaxType, decimal>
            {
                [TaxType.High] = _taxRateService.GetTaxRate(TaxType.High, request),
                [TaxType.Low] = _taxRateService.GetTaxRate(TaxType.Low, request),
                [TaxType.NoTax] = 0m
            };

            var productLines = _productLineService.GetProductLines(request.ProductLines);

            var lowestLineTax = productLines.Any() 
                ? new LowestLineTax(productLines.Min(o => o.TaxType))
                : new LowestLineTax(TaxType.High);

            var shipping = _shippingService.GetShipping(request.Shipping, lowestLineTax, productLines);

            var productionSpeed = _productionSpeedService.GetProductionSpeed(request.ProductionMethod, lowestLineTax, productLines);

            var paymentMethod = _paymentMethodService.GetPaymentMethod(request.PaymentMethod);

            var discountCollection = _discountService.GetAllApplicableDiscounts(request.Discounts, productLines, shipping, productionSpeed, paymentMethod);

            // TODO removed disabled discounts from discountCollection;

            return _calculationResultService.CreateResult(taxRateMap, productLines, shipping, productionSpeed, paymentMethod, discountCollection);
        }
    }
}
