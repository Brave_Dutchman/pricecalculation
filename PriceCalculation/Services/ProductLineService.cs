﻿using PriceCalculation.Models;
using PriceCalculation.Models.Request;
using System;
using System.Collections.Generic;

namespace PriceCalculation.Services
{
    public class ProductLineService
    {
        public IReadOnlyList<ProductLine> GetProductLines(IReadOnlyList<ProductLineRequest> requestProductLines)
        {
            var lines = new List<ProductLine>();

            foreach (var requestLine in requestProductLines)
            {
                lines.Add(new ProductLine(
                    requestLine: requestLine,
                    price: _getProductPrice(requestLine),
                    taxType: _getValidatedTaxType(requestLine.Pricing.TaxType),
                    amount: requestLine.Amount
                ));
            }

            return lines;
        }

        private TaxType _getValidatedTaxType(TaxType taxType)
        {
            if (taxType == TaxType.Invalid)
            {
                throw new InvalidOperationException("TaxType.Invalid is not supported");
            }

            return taxType;
        }

        private decimal _getProductPrice(ProductLineRequest request)
        {
            var price = request.Pricing.BasePriceIncl;

            if (request.Pages > request.Pricing.IncludedBasePages)
            {
                var extraPages = request.Pages - request.Pricing.IncludedBasePages;
                if (extraPages > 0)
                {
                    price = extraPages * request.Pricing.ExtraPagePriceIncl;
                }
            }

            return price * request.Amount;
        }
    }
}
