﻿using PriceCalculation.Models;
using PriceCalculation.Models.Request;
using System.Collections.Generic;
using System.Linq;

namespace PriceCalculation.Services
{
    public class ProductionSpeedService
    {
        public ProductionSpeed GetProductionSpeed(ProductionSpeedRequest productionSpeedRequest, LowestLineTax lowestLineTax, IReadOnlyCollection<ProductLine> productLines)
        {
            return new ProductionSpeed(
                price: _getProductionSpeedPrice(productionSpeedRequest, productLines),
                taxType: lowestLineTax.TaxType
            );
        }

        private decimal _getProductionSpeedPrice(ProductionSpeedRequest productionSpeedRequest, IReadOnlyCollection<ProductLine> productLines)
        {
            var price = 0m;

            if (productionSpeedRequest.IsRush)
            {
                var productionLinesdTotal = productLines.Sum(pl => pl.Price);
                price =  productionSpeedRequest.BasePriceIncl + (productionLinesdTotal * productionSpeedRequest.PercentageOfOrderLinePrice);
            }

            return price;
        }
    }
}
