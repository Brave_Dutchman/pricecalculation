﻿using PriceCalculation.Models;
using PriceCalculation.Models.Request;

namespace PriceCalculation.Services
{
    public class PaymentMethodService
    {
        public PaymentMethod GetPaymentMethod(PaymentMethodRequest paymentMethodRequest)
        {
            return new PaymentMethod(
                price: _getPaymentMethodCosts(paymentMethodRequest),
                taxType: TaxType.High
            );
        }

        private decimal _getPaymentMethodCosts(PaymentMethodRequest paymentMethodRequest)
        {
            return paymentMethodRequest.PriceIncl;
        }
    }
}
