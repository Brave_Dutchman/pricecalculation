﻿using PriceCalculation.Models;
using PriceCalculation.Models.Request;
using System.Collections.Generic;
using System.Linq;

namespace PriceCalculation.Services
{
    public class ShippingService
    {
        public Shipping GetShipping(ShippingRequest shipping, LowestLineTax lowestLineTax, IReadOnlyList<ProductLine> productLines)
        {
            return new Shipping(
                price: _getShippingPrice(shipping, productLines),
                taxType: lowestLineTax.TaxType,
                minOrderPriceForFreeShipping: shipping.MinOrderPriceForFreeShipping
            );
        }

        private decimal _getShippingPrice(ShippingRequest shipping, IReadOnlyList<ProductLine> productLines)
        {
            var method = shipping.Method;
            var shippingPrice = method.PriceIncl;

            if (method.ShippingUpPrice > 0)
            {
                var linesUpPrice = productLines.Select(pl => pl.RequestLine).Sum(l => l.Amount * l.ShippingUpPrice);
                shippingPrice += linesUpPrice - method.ShippingUpPrice;
            }

            if (shipping.MaxShippingCosts != null && shippingPrice > shipping.MaxShippingCosts.Value)
            {
                shippingPrice = shipping.MaxShippingCosts.Value;
            }

            return shippingPrice;
        }
    }
}
