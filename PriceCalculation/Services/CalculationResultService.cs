﻿using PriceCalculation.Models;
using PriceCalculation.Models.Result;
using System.Collections.Generic;
using System.Linq;

namespace PriceCalculation.Services
{
    public class CalculationResultService
    {
        public CalculationResult CreateResult(
            IReadOnlyDictionary<TaxType, decimal> taxRates,
            IReadOnlyList<ProductLine> productLines,
            Shipping shipping,
            ProductionSpeed productionSpeed,
            PaymentMethod paymentMethod,
            DiscountCollection discountCollection)
        {
            var productLinesResult = productLines.Select(pl => _createLine(pl, DiscountOwner.ProductLine, taxRates, discountCollection)).ToList();
            var shippingResult = _createLine(shipping, DiscountOwner.Shipping, taxRates, discountCollection);
            var productionSpeedResult = _createLine(productionSpeed, DiscountOwner.ProductionSpeed, taxRates, discountCollection);
            var paymentMethodResult = _createLine(paymentMethod, DiscountOwner.PaymentMethod, taxRates, discountCollection);

            var resultLines = productLinesResult.Concat(new[] { shippingResult, productionSpeedResult, paymentMethodResult }).ToList();

            return new CalculationResult(
                total: resultLines.Sum(t => t.AmountInclTax),
                taxRates: taxRates,
                productLines: productLinesResult,
                shipping: shippingResult,
                productionSpeed: productionSpeedResult,
                paymentMethod: paymentMethodResult,
                taxes: resultLines
                    .GroupBy(l => l.TaxType)
                    .Select(l => new CalculationResultTaxLine(l.Key, l.Sum(i => i.TaxAmount)))
                    .ToList(),
                discounts: resultLines
                    .GroupBy(l => l.TaxType)
                    .Select(l => new CalculationResulDiscountLine(l.Key, l.Sum(i => i.DiscountAmountInclTax), l.Sum(i => i.DiscountTaxAmount)))
                    .ToList()
            );
        }

        private CalculationResultLine _createLine(ICalculationItem calculationItem, DiscountOwner owner, IReadOnlyDictionary<TaxType, decimal> taxRates, DiscountCollection discountCollection)
        {
            var discounts = discountCollection.GetDiscountsOf(owner, calculationItem.Id).ToList();

            var flatDiscount = discounts.Where(d => d.Amount != null).Sum(d => d.Amount!.Value);

            var priceInclFlatDiscount = calculationItem.Price - flatDiscount;

            var discountPercentage = discounts.Where(d => d.Percentage != null).Sum(d => d.Percentage!.Value);
            var totalDiscount = priceInclFlatDiscount + (priceInclFlatDiscount * discountPercentage);

            if (totalDiscount > calculationItem.Price)
            {
                totalDiscount = calculationItem.Price;
            }

            var amountInclTax = calculationItem.Price - totalDiscount;
            var taxAmount = amountInclTax - (amountInclTax / (1 + taxRates[calculationItem.TaxType]));

            var discountTaxAmount = totalDiscount - (totalDiscount / (1 + taxRates[calculationItem.TaxType]));

            return new CalculationResultLine(
                calculationItem.Id,
                amountInclTax,
                taxAmount,
                calculationItem.TaxType,
                totalDiscount,
                discountTaxAmount
            );
        }
    }
}
