﻿using PriceCalculation.Models;
using PriceCalculation.Models.Request;
using PriceCalculation.Providers;
using System;

namespace PriceCalculation.Services
{
    public class TaxRateService
    {
        private readonly TaxRateProvider _taxRateProvider;

        public TaxRateService(TaxRateProvider taxRateProvider)
        {
            _taxRateProvider = taxRateProvider;
        }

        public decimal GetTaxRate(TaxType taxType, CalculationRequest request)
        {
            var requestedOn = request.RequestedOn;
            var invoiceCountry = request.InvoiceCountry;

            if (request.SupportedTaxRateCountries.Contains(invoiceCountry))
            {
                var taxRate = _taxRateProvider.GetTaxRate(invoiceCountry, taxType, requestedOn);
                if (taxRate != null)
                {
                    return taxRate.Value;
                }
            }

            if (!request.SupportedTaxRateCountries.Contains(request.ShopCountry))
            {
                throw new NotImplementedException();
            }

            var shopTaxRate = _taxRateProvider.GetTaxRate(request.ShopCountry, taxType, requestedOn);
            if (shopTaxRate == null)
            {
                throw new NotImplementedException();
            }

            return shopTaxRate.Value;
        }
    }
}
