﻿using PriceCalculation.Calculators.Discounts;
using PriceCalculation.Models;
using PriceCalculation.Models.Request;
using System.Collections.Generic;

namespace PriceCalculation.Services
{
    public class DiscountService
    {
        private readonly FreeShippingCalculator _freeShippingCalculator;
        private readonly ProductLineDiscountCalculator _productLineDiscountCalculator;
        private readonly ProductLineVolumeDiscountCalculator _productLineVolumeDiscountCalculator;
        private readonly OrderWideDiscountCalculator _orderWideDiscountCalculator;

        public DiscountService(
            FreeShippingCalculator freeShippingCalculator, 
            ProductLineDiscountCalculator productLineDiscountCalculator, 
            ProductLineVolumeDiscountCalculator productLineVolumeDiscountCalculator,
            OrderWideDiscountCalculator orderWideDiscountCalculator)
        {
            _freeShippingCalculator = freeShippingCalculator;
            _productLineDiscountCalculator = productLineDiscountCalculator;
            _productLineVolumeDiscountCalculator = productLineVolumeDiscountCalculator;
            _orderWideDiscountCalculator = orderWideDiscountCalculator;
        }

        public DiscountCollection GetAllApplicableDiscounts(
            IReadOnlyCollection<DiscountRequest> discounts,
            IReadOnlyList<ProductLine> productLines,
            Shipping shipping,
            ProductionSpeed productionSpeed,
            PaymentMethod paymentMethod)
        {
            var discountCollection = new DiscountCollection();

            discountCollection.AddRange(_freeShippingCalculator.GetFreeShippingDiscountIfApplies(shipping, productLines, discounts));
            discountCollection.AddRange(_productLineDiscountCalculator.GetProductDiscounts(discounts, productLines));
            discountCollection.AddRange(_productLineVolumeDiscountCalculator.GetProductVolumeDiscounts(productLines));
            discountCollection.AddRange(_orderWideDiscountCalculator.GetOrderWideDiscounts(discounts, productLines, shipping, productionSpeed, paymentMethod));

            return discountCollection;
        }
    }
}
