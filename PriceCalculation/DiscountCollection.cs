﻿using PriceCalculation.Models;
using System.Collections.Generic;
using System.Linq;

namespace PriceCalculation
{
    public class DiscountCollection
    {
        private readonly List<Discount> _discounts = new();

        public void AddRange(IEnumerable<Discount> discounts)
        {
            _discounts.AddRange(discounts);
        }

        public Dictionary<(DiscountOwner Owner, string OwnerId), IReadOnlyList<Discount>> GetGroupedDiscounts()
        {
            return _discounts
                .GroupBy(d => (d.Owner, d.OwnerId))
                .ToDictionary(g => g.Key, g => (IReadOnlyList<Discount>) g.ToList());
        }

        public IEnumerable<Discount> GetDiscountsOf(DiscountOwner owner, string ownerId)
        {
            foreach (var discount in _discounts)
            {
                if (discount.Owner == owner && discount.OwnerId == ownerId)
                {
                    yield return discount;
                }
            }
        }
    }
}
