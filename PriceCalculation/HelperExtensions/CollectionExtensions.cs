﻿using System.Linq;

namespace System.Collections.Generic
{
    public static class CollectionExtensions
    {
        public static bool ContainsAny<T>(this IReadOnlyCollection<T> collection, IReadOnlyCollection<T> other, IEqualityComparer<T> comparer = null)
        {
            comparer = comparer ?? EqualityComparer<T>.Default;

            foreach (var item in other)
            {
                if (collection.Contains(item, comparer))
                {
                    return true;
                }
            }

            return false;
        }
    }
}
