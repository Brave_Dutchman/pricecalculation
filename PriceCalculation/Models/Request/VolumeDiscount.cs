﻿namespace PriceCalculation.Models.Request
{
    public class VolumeDiscount
    {
        public string Id { get; set; }

        public decimal Percentage { get; set; }
    }
}
