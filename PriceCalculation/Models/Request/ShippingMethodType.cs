﻿namespace PriceCalculation.Models.Request
{
    public enum ShippingMethodType
    {
        Standard = 0,
        PickupFF = 1,
        PostNL = 2,
        Courier = 3,
        AddressFile = 4
    }
}
