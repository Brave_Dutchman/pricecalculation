﻿namespace PriceCalculation.Models.Request
{
    public class ProductPricingRequest
    {
        public decimal BasePriceIncl { get; set; }

        public decimal ExtraPagePriceIncl { get; set; }

        public int IncludedBasePages { get; set; }

        public TaxType TaxType { get; set; }
    }
}
