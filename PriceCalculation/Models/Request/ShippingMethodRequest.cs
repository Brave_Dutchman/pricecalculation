﻿namespace PriceCalculation.Models.Request
{
    public class ShippingMethodRequest
    {
        public ShippingMethodType Type { get; set; }

        public decimal PriceIncl { get; set; }

        public decimal ShippingUpPrice { get; set; }
    }
}
