﻿using System.Collections.Generic;

namespace PriceCalculation.Models.Request
{
    public class DiscountRequest
    {
        public string Id { get; set; }

        public DiscountName DiscountName { get; set; }

        public bool HasFreeShipping { get; set; }

        public bool DisableVolumeDiscount { get; set; }

        public bool DisablesPromotions { get; set; }

        /// <summary>
        /// Determines whether the <see cref="Amount"/> is a fixed amount or a percentage between 0 and 100
        /// </summary>
        public bool IsAmountPercentage { get; set; }

        /// <summary>
        /// Usage and containing information determined by <see cref="IsAmountPercentage"/>
        /// </summary>
        public decimal Amount { get; set; }

        public DiscountScope Scope { get; set; }

        public bool ScopeIncludesShipping { get; set; }

        public IReadOnlyCollection<string> Tags { get; set; }

        public IReadOnlyList<DiscountName> DisablesTypes()
        {
            var list = new List<DiscountName>();

            if (DisableVolumeDiscount)
            {
                list.Add(DiscountName.VolumeDiscount);
            }

            if (DisablesPromotions)
            {
                list.Add(DiscountName.PromotionOrder);
                list.Add(DiscountName.PromotionProduct);
            }

            return list;
        }
    }
}
