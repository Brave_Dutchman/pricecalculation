﻿namespace PriceCalculation.Models.Request
{
    public class PaymentMethodRequest
    {
        public decimal PriceIncl {get; set; }
    }
}
