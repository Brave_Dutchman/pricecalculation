﻿namespace PriceCalculation.Models.Request
{
    public class ProductionSpeedRequest
    {
        public bool IsRush { get; set; }

        public decimal BasePriceIncl { get; set; }

        public decimal PercentageOfOrderLinePrice { get; set; }
    }
}
