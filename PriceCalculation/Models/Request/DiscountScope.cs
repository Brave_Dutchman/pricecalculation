﻿namespace PriceCalculation.Models.Request
{
    public enum DiscountScope
    {
        ProductLine,
        Order
    }
}
