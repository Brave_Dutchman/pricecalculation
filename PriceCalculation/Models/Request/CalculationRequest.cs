﻿using System;
using System.Collections.Generic;

namespace PriceCalculation.Models.Request
{
    public class CalculationRequest
    {
        public DateTime RequestedOn { get; set; }

        public Country InvoiceCountry { get; set; }

        public Country ShopCountry { get; set; }

        public bool PricesAreInclusiveVat { get; set; }

        public HashSet<Country> SupportedTaxRateCountries { get; set; }

        public ShippingRequest Shipping { get; set; }

        public ProductionSpeedRequest ProductionMethod { get; set; }

        public PaymentMethodRequest PaymentMethod { get; set; }

        public IReadOnlyList<ProductLineRequest> ProductLines { get; set; }

        public IReadOnlyList<DiscountRequest> Discounts { get; set; }
    }
}
