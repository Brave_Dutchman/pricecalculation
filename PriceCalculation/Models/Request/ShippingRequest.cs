﻿namespace PriceCalculation.Models.Request
{
    public class ShippingRequest
    {
        public decimal? MinOrderPriceForFreeShipping { get; set; }

        public decimal? MaxShippingCosts { get; set; }

        public ShippingMethodRequest Method { get; set; }
    }
}
