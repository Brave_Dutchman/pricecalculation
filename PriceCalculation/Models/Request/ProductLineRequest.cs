﻿using System.Collections.Generic;

namespace PriceCalculation.Models.Request
{
    public class ProductLineRequest
    {
        public ProductLineRequest(string id)
        {
            Id = id;
        }

        public string Id { get; }

        public ProductPricingRequest Pricing { get; set; }

        public int Amount { get; set; }

        public int Pages { get; set; }

        public decimal ShippingUpPrice { get; set; }

        public IReadOnlyCollection<string> Tags { get; set; }

        public VolumeDiscount VolumeDiscount { get; set; }
    }
}
