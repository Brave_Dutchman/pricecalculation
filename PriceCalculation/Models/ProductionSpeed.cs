﻿using System;

namespace PriceCalculation.Models
{
    public class ProductionSpeed : ICalculationItem
    {
        public ProductionSpeed(decimal price, TaxType taxType)
        {
            Id = Guid.NewGuid().ToString();
            Price = price;
            TaxType = taxType;
        }

        public string Id { get; }

        /// <summary>
        /// The price of shipping excluding tax and discounts
        /// </summary>
        public decimal Price { get; }

        public TaxType TaxType { get; }
    }
}