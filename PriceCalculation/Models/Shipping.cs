﻿using System;

namespace PriceCalculation.Models
{
    public class Shipping : ICalculationItem
    {
        public Shipping(decimal price, TaxType taxType, decimal? minOrderPriceForFreeShipping)
        {
            Id = Guid.NewGuid().ToString();

            Price = price;
            TaxType = taxType;
            MinOrderPriceForFreeShipping = minOrderPriceForFreeShipping;
        }

        public string Id { get; }

        /// <summary>
        /// The price of shipping excluding tax and discounts
        /// </summary>
        public decimal Price { get; }

        public TaxType TaxType { get; }

        public decimal? MinOrderPriceForFreeShipping { get; }
    }
}
