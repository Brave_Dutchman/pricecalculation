﻿namespace PriceCalculation.Models
{
    public enum DiscountName
    {
        Invalid,

        PromotionOrder,
        VoucherOrder,

        PromotionProduct,
        VoucherProduct,

        FreeShipping,
        FreeShippingLinesSum,
        VolumeDiscount
    }
}
