﻿namespace PriceCalculation.Models
{
    public interface ICalculationItem
    {
        string Id { get; }
        decimal Price { get; }
        TaxType TaxType { get; }
    }
}