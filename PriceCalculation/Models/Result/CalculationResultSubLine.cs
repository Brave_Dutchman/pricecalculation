﻿namespace PriceCalculation.Models.Result
{
    public class CalculationResultTaxLine
    {
        public CalculationResultTaxLine(TaxType taxType, decimal taxAmount)
        {
            TaxAmount = taxAmount;
            TaxType = taxType;
        }

        public decimal TaxAmount { get; set; }

        public TaxType TaxType { get; }
    }
}