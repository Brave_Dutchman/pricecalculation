﻿using System.Collections.Generic;

namespace PriceCalculation.Models.Result
{
    public class CalculationResult
    {
        public CalculationResult(
            decimal total,
            IReadOnlyDictionary<TaxType, decimal> taxRates,
            IReadOnlyList<CalculationResultLine> productLines,
            CalculationResultLine shipping,
            CalculationResultLine productionSpeed,
            CalculationResultLine paymentMethod,
            IReadOnlyList<CalculationResultTaxLine> taxes,
            IReadOnlyList<CalculationResulDiscountLine> discounts)
        {
            Total = total;
            TaxRates = taxRates;
            ProductLines = productLines;
            Shipping = shipping;
            ProductionSpeed = productionSpeed;
            PaymentMethod = paymentMethod;
            Taxes = taxes;
            Discounts = discounts;
        }

        public decimal Total { get; }

        public IReadOnlyDictionary<TaxType, decimal> TaxRates { get; }

        public IReadOnlyList<CalculationResultLine> ProductLines { get; }

        public CalculationResultLine Shipping { get; }

        public CalculationResultLine ProductionSpeed { get; }

        public CalculationResultLine PaymentMethod { get; }

        public IReadOnlyList<CalculationResultTaxLine> Taxes { get; }

        public IReadOnlyList<CalculationResulDiscountLine> Discounts { get; }
    }
}