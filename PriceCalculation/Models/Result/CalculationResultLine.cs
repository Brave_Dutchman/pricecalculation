﻿namespace PriceCalculation.Models.Result
{
    public class CalculationResultLine
    {
        public CalculationResultLine(
            string id, 
            decimal amountInclTax, 
            decimal taxAmount, 
            TaxType taxType, 
            decimal discountAmountInclTax, 
            decimal discountTaxAmount)
        {
            Id = id;
            AmountInclTax = amountInclTax;
            TaxAmount = taxAmount;
            TaxType = taxType;
            DiscountAmountInclTax = discountAmountInclTax;
            DiscountTaxAmount = discountTaxAmount;
        }

        public string Id { get; init; }

        public decimal AmountInclTax { get; }

        public decimal TaxAmount { get; }

        public TaxType TaxType { get; }

        public decimal DiscountAmountInclTax { get; }

        public decimal DiscountTaxAmount { get; }
    }
}