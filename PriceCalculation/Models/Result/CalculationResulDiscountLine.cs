﻿namespace PriceCalculation.Models.Result
{
    public class CalculationResulDiscountLine
    {
        public CalculationResulDiscountLine(TaxType taxType, decimal amountInclTax, decimal taxAmount)
        {
            TaxType = taxType;
            AmountInclTax = amountInclTax;
            TaxAmount = taxAmount;
        }

        public TaxType TaxType { get; }

        public decimal AmountInclTax { get; set; }

        public decimal TaxAmount { get; set; }
    }
}