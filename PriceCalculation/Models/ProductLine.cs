﻿using PriceCalculation.Models.Request;

namespace PriceCalculation.Models
{
    public class ProductLine : ICalculationItem
    {
        public ProductLine(
            ProductLineRequest requestLine,
            decimal price, 
            TaxType taxType, 
            decimal amount)
        {
            Price = price;
            TaxType = taxType;
            Amount = amount;
            RequestLine = requestLine;
        }

        public string Id => RequestLine.Id;

        /// <summary>
        /// The price of this productline excluding tax and discounts
        /// </summary>
        public decimal Price { get; }

        public TaxType TaxType { get; }

        public decimal Amount { get; }

        public ProductLineRequest RequestLine { get; }

    }
}
