﻿using System;
using System.Collections.Generic;

namespace PriceCalculation.Models
{
    public class Discount
    {
        public Discount(
            string ownerId,
            DiscountOwner owner,
            string identifier, 
            DiscountName name,
            decimal? amount,
            decimal? percentage,
            IReadOnlyCollection<DiscountName> disablesTypes)
        {
            if (amount == null && percentage == null)
            {
                throw new ArgumentException($"{nameof(amount)} or {nameof(percentage)} are required");
            }
            else if (amount != null && percentage != null)
            {
                throw new ArgumentException($"only one of {nameof(amount)} and {nameof(percentage)} are allowed with a non null value");
            }

            OwnerId = ownerId;
            Owner = owner;

            Identifier = identifier;
            Name = name;
            Amount = amount;
            Percentage = percentage;
            DisablesTypes = disablesTypes;
        }

        public string Identifier { get; }

        public string OwnerId { get; set; }

        public DiscountOwner Owner { get; set; }

        public DiscountName Name { get; }

        public decimal? Percentage { get; }

        public decimal? Amount { get; }

        public IReadOnlyCollection<DiscountName> DisablesTypes { get; }
    }
}
