﻿namespace PriceCalculation.Models
{
    public enum TaxType
    {
        Invalid = 0,
        NoTax = 1,
        Low = 2,
        High = 3
    }
}
