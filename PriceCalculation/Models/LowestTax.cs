﻿namespace PriceCalculation.Models
{
    public struct LowestLineTax
    {
        public LowestLineTax(TaxType taxType)
        {
            TaxType = taxType;
        }

        public TaxType TaxType { get; }
    }
}
