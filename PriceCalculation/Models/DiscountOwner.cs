﻿namespace PriceCalculation.Models
{
    public enum DiscountOwner
    {
        ProductLine,
        Shipping,
        ProductionSpeed,
        PaymentMethod
    }
}
