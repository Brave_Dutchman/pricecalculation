﻿using PriceCalculation.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace PriceCalculation.Providers
{
    public class TaxRateProvider
    {
        private IReadOnlyDictionary<Country, IReadOnlyList<CountryTaxRate>> _countryTaxRate 
            = new Dictionary<Country, IReadOnlyList<CountryTaxRate>>();

        public decimal? GetTaxRate(Country country, TaxType taxType, DateTime requestedOn)
        {
            if (_countryTaxRate.TryGetValue(country, out var revisions))
            {
                return revisions.Where(r => r.TaxType == taxType).FirstOrDefault(r => r.ActiveOn <= requestedOn)?.TaxRate;
            }

            return null;
        }

        public class CountryTaxRate
        {
            public Country Country { get; set; }

            public DateTime ActiveOn { get; set; }

            public TaxType TaxType { get; set; }

            public decimal TaxRate { get; set; }
        }
    }
}
